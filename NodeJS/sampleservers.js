var http = require('http');

function dealWithWebRequest(request,response){
	response.writeHead(200,{"Content-Type" : "text/plain"});
	response.write("Hello Node.js mmmmm");
	response.end();
}

var webserver = http.createServer(dealWithWebRequest).listen(8124,"127.0.0.1");
webserver.once('listening',function() {
	console.log('Server running at http://127.0.0.1:8124/');
});

//Node.js Event Driven Programming

var file = fileSystem.read("big.txt");
console.log(file.toString());

var takeAnotherFile = fileSystem.read("anotherbigfile.txt");
console.log(takeAnotherFile.toString());

fileSystem.read("big.txt",function(data){
	console.log(data.toString());
});

fileSystem.read("anotherbigfile.txt", function(anotherdata){
	console.log(another.toString());
});

//--------------------------------------------------------

/*
*Code snippet for asynchronous programming**
*/
var callback1 = function(){
	//return to the response
};
thisIsAsychronousCall(callback1);
//--------------
var callback2 = function(){
	//return to the response
};
Promise.when(thisIsAsychronousCall()).then(callback2);
//--------------

/***************************
introduce Node.js
****************************/
/*
The .on method
*/
var fs = require('fs');//get the fs module
var readStream = fs.createReadStream('/etc/passwd');

readStream.on('data', function(data){
	console.log(data);
});

readStream.on('end',function(){
	console.log('file ended');
});
////////////////////////////////////////////////////////
/*
The .once method
*/
server.once('connection', function(stream){
	console.log('We have our first call!');
});

function connectAndNotify(stream) {
console.log('We have our first call!');
server.removeListener('connection', connectAndNotify);
}
server.on('connection',connectAndNotify);

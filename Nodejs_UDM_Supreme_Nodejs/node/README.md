
What is REPL?

A Read-Eval-Print Loop(REPL) is an interactive interpreter to a programming language. It originated with LISP
system,but many other language (Python,Ruby,Huskell ,Tcl,etc.) use REPL's to manage interactive sessions.
They allow for simple experimentation with a language by bypassing th complie stage of the "Code -> complie -> execute" cycle.


RELP 4 Component 
1. read - which reads input from the keyboard
2. eval - which evaluates code passed to it
3. print - which to display results
4. Loop - which runs the three previous commands until termination

What is Crypto?

- Crypto module can encrypt and decrypt the data.
- Just like password encryption.

Crypto Classes

- Crypto
- Cipher
- Decipher
- Diffiehellman
- ECDH(Eilliptic Curve Diffie-Hellman)
- Hash
- Hmac
- Sign
- Verify

Some of Cipher Algorithm List

- cast-cbc
- aes-128-cbc
- aes-128-cbc-hmac-sha1
- aes-128-cfb
- aes256
- blowfish
- cast
- idea
- rc2 
- rc2-40-cbc
- rc4
- rc4-hmac-md5
- seed
- seed-cbc
- seed-ofb

Some of Hashes Algorithm List
- DSA
- DSA-SHA
- RSA-MD4
- md5
- sha1
- sha256
- sha512
- shaWithRSAEncryption
- ssl2-md5
- ripemd
- rmd160
- mdc2
- dss1
- ecdsa-with-SHA1

File System nodejs

What is File System?

- File I/O is provided by simple wrappers around standard POSIX functions.
- All the methods have asynchronous and synchronous forms.
- To use this module do require('fs')
- the asynchronous form allways takes completion callback as its last argument
- The synchronous form is any exceptions are immediately thrown.

Stream

What is Stream?
it is an abstract interface for working with streaming data.

Stream can be readable and writable.

Types of Stream
- Readable
>> Data can be read.
>> Example code
>> fs.createReadStream()

- Writaable
>> Data can be written.
>> Example code
>> fs.createWriteStream()

- Duplex
>> Data can both Readable and Writable
>> Example code
>> net.socket()

- Transform
>> Data can modify or transform.
>> Example code
>> Zlib.createDeflate()

What is buffer?

- A Buffer is a chunk of memory.
- It can interpret memory as an array of integer.
- And Floating numbers.
- The Buffer class is a global.

Mongo DB
install mongodb 
Add path >> Addvance system setting >> path >> Environment Varibles

add path mongo by install

install 
Run Server and Mongodb Setup
mongod --dbpath /c/Users/name/Documents/GitHub/Node-course/node/13-mongodb/crud/database/

go to path
directory 

/c/Users/name/Documents/GitHub/Node-course/node/13-mongodb/crud/database/

$ mongo
use crud


CRUD Express
CRUD Create Crud using Express with Environmet Setup

express crud 
cd crud
npm install mongodb --save

insert data to database
//add database  app.js
var mongo = require('mongodb');

go to index.js 
add //
var mongodb = require('mongodb');

find data 
db.allcars.find().pretty()

View Data From the database

add in index.js
```
router.get('/listcars', function(req, res) {
    var MongoClient = mongodb.MongoClient;
    var url = 'mongodb://localhost:27017/crud';
    MongoClient.connect(url, function(err, db) {
        if (err) {
            console.log(err);
        } else {
            console.log('connected Successfully');
            var collection = db.collection('allcars');
            collection.find({}).toArray(function(err, result) {
                // body
                if (err) {
                    res.send(err);
                } else if (result.length) {
                    res.render('listcar', {
                        'listcar': result
                    });
                } else {
                    res.send('No cars');
                }
                db.close();
            });
        }
    });
});

```

View Specific ID from the database
```

router.get('/editcars/:id', function(req, res) {
    // body
    var id = req.params.id;
    var MongoClient = mongodb.MongoClient;
    var url = 'mongodb://localhost:27017/crud';

    MongoClient.connect(url, function(err, db) {
        // body
        if (err) {
            console.log("this is first error" + err);
        } else {
            var collection = db.collection('allcars');
            collection.find({ _id: ObjectId(id) }).toArray(function(err, result) {
                // body
                if (err) {
                    res.send(err);
                } else if (result.length) {
                    res.render('editcars', { title: 'edit car', 'listcar': result });
                } else {
                    res.send('No Cars');
                }
                db.close();
            });
        }
    });
});
```

Update Data From the database

Login and Registration


Nodemon - Automatic refresh the server

npm install nodemon -g


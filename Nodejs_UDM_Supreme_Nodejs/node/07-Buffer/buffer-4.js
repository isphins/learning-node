//Compare Buffer

var b1 = new Buffer('hello');
var b2 = new Buffer('hello');
var compare = b1.compare((b2));

if (compare == 0) {
    console.log(b1 + " is same as " + b2);
} else {
    console.log(b1 + " is not the same as " + b2);
}
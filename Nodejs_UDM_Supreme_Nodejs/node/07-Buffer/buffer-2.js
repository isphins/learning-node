//Reading Buffers

var buffer = new Buffer(30);

for (var i = 0; i < 30; i++) {
    buffer[i] = i + 100;
}
console.log(buffer.toString('ascii'));
console.log(buffer.toString('ascii', 0, 10));
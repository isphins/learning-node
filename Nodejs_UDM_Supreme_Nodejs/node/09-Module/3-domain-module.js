var d = require("domain").create();

d.on('error', (er) => {
    console.log("error", er.message);
});

d.run(() => {
    require('http').createServer((req, res) => {
        handleRequest(req, res);
    }).listen(PORT);
});
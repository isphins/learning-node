var os = require("os");

console.log("endianness : " + os.endianness());
console.log("type : " + os.type());
console.log("plateform : " + os.platform());
console.log("Operating system version " + os.release());
console.log("total memory : " + os.totalmem() + " bytes");
console.log("free memory : " + os.freemem() + " bytes");
console.log("hostname : " + os.hostname());
console.log("uptime : " + os.uptime());
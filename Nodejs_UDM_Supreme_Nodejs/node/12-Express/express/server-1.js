//Express Routing

var express = require('express');

var z = express();


// Access assets folder (img,js,css)
z.use(express.static('assets'));
z.get('/', function(req, res) {
    // body
    res.send('<img src="img/cat.png">');
});

z.get('/sample', function(req, res) {
    // body
    // res.send('Welcome again to express');
    //Access Html File
    res.sendFile(__dirname + '/index.html');
});

z.listen(8044, function() {
    // body
    console.log("visit at localhost:8044");
});

z.use(require('./server-2'));
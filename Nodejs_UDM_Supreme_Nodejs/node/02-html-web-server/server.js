var http = require('http'),
    z = require('fs');

z.readFile('index.html', function(err, html) {
    if (err) {
        throw err;
    }

    http.createServer(function(request, response) {
        response.writeHead(200, { "Content-Type": "text/html" });
        response.write(html);
        response.end();
    }).listen(8550);
    console.log("Server running at http://localhost:8550");
});
var express = require('express');
var router = express.Router();

//add database
var mongodb = require('mongodb');
//View Specific ID from the database
var ObjectId = require('mongodb').ObjectID;
//Register Account
const crypto = require('crypto');
/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', { title: 'Artdvp' });
});


router.get('/addcar', function(req, res, next) {
    res.render('cars', { title: 'add car' });
});

router.post('/add_success', function(req, res) {
    // body
    var MongoClient = mongodb.MongoClient;
    var url = 'mongodb://localhost:27017/crud';
    MongoClient.connect(url, function(err, db) {
        if (err) {
            console.log(err);
        } else {
            console.log('connected');
            var collection = db.collection('allcars');
            var carlist = { brand: req.body.brand, color: req.body.color };

            collection.insert([carlist], function(err, result) {
                // body
                if (err) {
                    console.log(err);
                } else {
                    res.render('success');
                    console.log('successfully added to the database');
                }
                db.close();
            });
        }
    });
});



//View Data From the database
router.get('/listcars', function(req, res) {
    var MongoClient = mongodb.MongoClient;
    var url = 'mongodb://localhost:27017/crud';
    MongoClient.connect(url, function(err, db) {
        if (err) {
            console.log(err);
        } else {
            console.log('connected Successfully');
            var collection = db.collection('allcars');
            collection.find({}).toArray(function(err, result) {
                // body
                if (err) {
                    res.send(err);
                } else if (result.length) {
                    res.render('listcar', {
                        'listcar': result
                    });
                } else {
                    res.send('No cars');
                }
                db.close();
            });
        }
    });
});

//View Specific ID from the database
router.get('/editcars/:id', function(req, res) {
    // body
    var id = req.params.id;
    var MongoClient = mongodb.MongoClient;
    var url = 'mongodb://localhost:27017/crud';

    MongoClient.connect(url, function(err, db) {
        // body
        if (err) {
            console.log("this is first error" + err);
        } else {
            var collection = db.collection('allcars');
            collection.find({ _id: ObjectId(id) }).toArray(function(err, result) {
                // body
                if (err) {
                    res.send(err);
                } else if (result.length) {
                    res.render('editcars', { title: 'edit car', 'listcar': result });
                } else {
                    res.send('No Cars');
                }
                db.close();
            });
        }
    });
});

//update data to database
router.post('/updatecars/:id', function(req, res) {

    var id = req.params.id;
    var MongoClient = mongodb.MongoClient;
    var url2 = 'mongodb://localhost:27017/crud';
    // body
    MongoClient.connect(url2, function(err, db) {
        if (err) {
            console.log("first error " + err);
        } else {
            console.log('connected');
            var collection = db.collection('allcars');
            collection.update({ _id: ObjectId(id) }, { $set: { brand: req.body.brand, color: req.body.color } }, function(err, result) {
                // body
                if (err) {
                    console.log(err);
                } else {
                    res.render("successupdate", { title: 'success' });
                }
                db.close();
            });
        }
    });
});


//Register Account
router.get('/register', function(req, res) {
    // body
    res.render('register', { title: 'add car' });
});

router.post('/reg_success', function(req, res) {
    // body
    var MongoClient = mongodb.MongoClient;
    var data = 'mongodb://localhost:27017/crud';
    MongoClient.connect(data, function(err, db) {
        if (err) {
            console.log("connection failed " + err);
        } else {
            console.log("connected");
            var collection = db.collection('newaccount');
            const password = req.body.password;
            const pass = crypto.createHmac('sha256', password).update('this is password').digest('hex');

            var accounts = { username: req.body.username, password: pass };
            collection.insert([accounts], function(err, result) {
                if (err) {
                    console.log(err);
                } else {
                    res.render('successreg');
                    console.log("successfully added");
                }
            });
        }
    });
});

//login with simple validation
router.get('/login', function(req, res) {
    // body
    res.render('login', { title: 'login' });
});
//home
router.get('/home', function(req, res) {
    // body
    res.render('home', { title: 'home' });
});


router.post('/validation', function(req, res) {
    // body
    var MongoClient = mongodb.MongoClient;
    var data4 = 'mongodb://localhost:27017/crud';
    MongoClient.connect(data4, function(err, db) {
        if (err) {
            console.log("connection failed " + err);
        } else {
            var collection = db.collection('newaccount');
            const username = req.body.username;
            const password = req.body.password;
            const pass = crypto.createHmac('sha256', password).update('this is password').digest('hex');

            collection.find({ username: username, password: pass }).toArray(function(err, result) {
                // body
                if (err) {
                    console.log(err);
                } else if (result.length) {
                    return res.redirect('/home');
                } else {
                    res.render('login', { 'notification': 'right' });
                }
                db.close();
            });
        }
    });
});

module.exports = router;
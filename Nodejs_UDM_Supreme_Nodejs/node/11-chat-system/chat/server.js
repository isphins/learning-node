var express = require('express');
var app = express();

var http = require("http").Server(app);
var io = require('socket.io')(http);

app.use(express.static('assets'));
app.get('/', function(req, res) {
    // body
    res.sendFile(__dirname + '/index.html');
})

io.on('connection', function(socket) {
    console.log("You are connected");

    socket.on('chat message', function(msg) {
        console.log("message : " + msg);
        io.emit('chat message', msg);
    });

    socket.on('disconnect', function() {
        console.log("user disconnect");
    });
});

http.listen(8099, function() {
    console.log("server is running at localhost:8099");
});
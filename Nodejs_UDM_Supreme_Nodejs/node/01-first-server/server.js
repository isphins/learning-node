var http = require('http');

function r_server(req, res) {
    var details = "Welcome to your first web server";
    var content_length = details.length;
    res.writeHead(200, {
        'Content-Length': content_length,
        'Content-Type': 'text/html'
    })
    res.end(details);
}

var s = http.createServer(r_server);
s.listen(8500);

console.log("You server is now running at http://localhost:8500/");
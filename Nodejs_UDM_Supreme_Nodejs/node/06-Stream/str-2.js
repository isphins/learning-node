//Stream Read Text file using Stream
var z = require('fs');
var details = "";

var stream = z.createReadStream("file.txt");

stream.setEncoding('UTF8');
stream.on('data', function(list) {
    details = list;
});

stream.on('end', function() {
    console.log(details);
});
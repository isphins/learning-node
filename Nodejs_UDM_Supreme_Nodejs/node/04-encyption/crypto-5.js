//Crypto Hash Class

const crypto = require('crypto');
const hash = crypto.createHash('md5');

hash.on('readable', () => {
    var data = hash.read();
    if (data) {
        console.log(data.toString('hex'));
    }
});

hash.write('This is a sample');
hash.end();
//Crypto Cipher

const crypto = require('crypto');
const cipher = crypto.createCipher('aes192', 'Password');

var encrypt = '';
cipher.on('readable', () => {
    var data = cipher.read();
    if (data) {
        encrypt += data.toString('hex');
    }
});

cipher.on('end', () => {
    console.log(encrypt);
});

cipher.write('this will be encrypted by cipher');
cipher.end();


//Decipher
const decipher = crypto.createDecipher('aes192', 'Password');
var decrypted = '';

decipher.on('readable', () => {
    var data = decipher.read();
    if (data) {
        decrypted += data.toString('utf8');
    }
});

decipher.on('end', () => {
    console.log(decrypted);
});

decipher.write(encrypt, 'hex');
decipher.end();